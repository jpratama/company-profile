import React from 'react';

import Layout from '../components/Layout';

import pic1 from '../assets/images/logo_pesona.jpg';
import pic2 from '../assets/images/logo_jasamarga.jpg';
import pic3 from '../assets/images/logo_medcalindo.jpg';
import pic4 from '../assets/images/04.jpg';
import pic5 from '../assets/images/logo_citius.jpg';

import { Link } from 'gatsby';

const IndexPage = () => (
  <Layout>
    <div id="main">
      <div className="inner">
        <header>
          <h1>Our Company</h1>
          <p>Being a leading System and IT Infrastructure Company to provide a solid Information Technology 
            for System, Security and Infrastructure as a key to get success of any business.</p>
          <p>Our System & Infrastructure experts can help identify needs, cut costs and improve productivity.
            <ol>
              <li>Security Services</li>
              <li>Advance Networking - Servers, PCs and Backbone Hardware</li>
              <li>Low Cost Enterprise Zimbra Mail Server</li>
              <li>Site to Site Interconnection</li>
              <li>Advance VPN System (Also support any basic/personal ISP such as Indihome, firstmedia etc)</li>
              <li>Backup and Disaster Recovery Solutions</li>
              <li>High Availability System</li>
              <li>Web Services</li>
            </ol>
          </p>
        </header>
        <section className="tiles">
          <article className="style1">
            <span className="image">
              <img src={pic1} alt="" />
            </span>
            <Link to="/Generic">
              <h2>PT. POJ (Pesona Optima Jasa)</h2>
              <div className="content">
                <p>Build System and Network environment for Call Center
                  Perform Wireless Network Installation
                  Setup and Configure Internet Gateway using Linux Server
                  Configure PC for VOIP Client</p>
              </div>
            </Link>
          </article>
          <article className="style2">
            <span className="image">
              <img src={pic2} alt="" />
            </span>
            <Link to="/Generic">
              <h2>PT. Jasa Marga</h2>
              <div className="content">
                <p>Troubleshoot DNS Failover Problem</p>
              </div>
            </Link>
          </article>
          <article className="style3">
            <span className="image">
              <img src={pic3} alt="" />
            </span>
            <Link to="/Generic">
              <h2>PT. Medcalindo</h2>
              <div className="content">
                <p>Enhance Wireless Network Enivironment System
                  -Perform Installation of wireless network with the new system
                  -Configure Linux and Samba File Sharing System
                  -Configure VPN for access office from remote</p>
                <p>Enhance Wireless Network System
                  -Perform Installation of wireless network with the new system
                  -Configure Linux and Samba File Sharing System
                  -Configure VPN for accessing office from remote</p>
              </div>
            </Link>
          </article>
          <article className="style4">
            <span className="image">
              <img src={pic4} alt="" />
            </span>
            <Link to="/Generic">
              <h2>PT. GilGal</h2>
              <div className="content">
                <p>Enhance System and Network Security of Cloud Server
                  -Collect data of system after hacked
                  -Secure the OS system
                  -Build VPN to secure the access to the server</p>
              </div>
            </Link>
          </article>
          <article className="style5">
            <span className="image">
              <img src={pic5} alt="" />
            </span>
            <Link to="/Generic">
              <h2>PT. Citius</h2>
              <div className="content">
                <p>Mail Server using Zimba Colaboration Suite
                  -Setup and configure Linux and Zimbra Server
                  -Apply Anti Spam and Anti Virus for the mail server
                  -Enhance security of the system</p>
              </div>
            </Link>
          </article>
        </section>
      </div>
    </div>
  </Layout>
);

export default IndexPage;
